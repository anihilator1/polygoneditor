﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graph_Editor_GK
{

    public class Vertex
    {
        public double X { get; set; }
        public double Y { get; set; }
        public Constraint Constraint { get; set; }
        public double Angle { get; set; }
        public Vertex(double x,double y)
        {
            this.X = x;
            this.Y = y;
            Constraint = Constraint.None;
        }
        public static Vertex operator +(Vertex a,Vector v )
        {
            a.X = a.X + v.X;
            a.Y = a.Y + v.Y;
            return a;
        }
        public static Vertex operator -(Vertex v1, Vertex v2)
        {
            return new Vertex(v1.X - v2.X, v1.Y -v2.Y);
        }
        public bool CloseToVertex(int x1, int y1)
        {
            if (Math.Pow((X - x1), 2) + Math.Pow(Y - y1, 2) < Math.Pow(20, 2)) return true;
            return false;
        }
        public static double CrossProduct(Vertex v1, Vertex v2)
        {
            return v1.X * v2.Y - v2.X * v1.Y;
        }

        public Vertex Clone()
        {
            return new Vertex(X, Y);
        }

        public Point ToPoint()
        {
            return new Point((int)X, (int)Y);
        }
         
    }
}
