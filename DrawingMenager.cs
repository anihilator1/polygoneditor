﻿using Graph_Editor_GK.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graph_Editor_GK
{
    class DrawingMenager
    {
        private PictureBox pictureBox;
        public Bitmap bitmap;
        private Bitmap verticalIcon;
        private Bitmap horizontalIcon;
        private Graphics graphics;
        private Color selectionColor = Color.Blue;
        private int r = 5;
        private int iconSize = 30;
        public DrawingMenager(PictureBox pictureBox)
        {
            this.bitmap = (Bitmap)pictureBox.Image;
            this.pictureBox = pictureBox;
            ResourceManager resourceMenager = Resources.ResourceManager;
            verticalIcon = (Bitmap)resourceMenager.GetObject("vertical_icon");
            horizontalIcon = (Bitmap)resourceMenager.GetObject("horizontal_icon");
            graphics = Graphics.FromImage(bitmap);
            graphics.Clear(Color.White);
        }
        public void DrawPoint(int x, int y, Color color, int radius)
        {
            Brush brush = new SolidBrush(color);
            Rectangle rectangle = new Rectangle(x - radius, y - radius, radius * 2, radius * 2);
            graphics.FillEllipse(brush,rectangle);
            brush.Dispose();
        }
        public void DrawVertex(Vertex a, Color color)
        {
            DrawPoint((int)a.X,(int) a.Y, color, r);
        }
        public void DrawLine(Vertex a, Vertex b, Color color, float size)
        {
            Pen pen = new Pen(color, size);
            graphics.DrawLine(pen, a.ToPoint(), b.ToPoint());
            pen.Dispose();           
        }
        public void DrawSide(Edge a, Color color, float size)
        {
            DrawLine(a.V1, a.V2, color, size);
        }
        private void RefreshImage()
        {
            pictureBox.CreateGraphics().DrawImageUnscaled(bitmap, new Point(0, 0));
        }


        public void DrawPolygon(Polygon polygon, Color color, float size)
        {
            graphics.Clear(Color.White);
            if (polygon == null) return;
            foreach (Edge edge in polygon.Edges())
            {
                if (polygon.EqualsSelected(edge))
                {
                    DrawSide(edge, selectionColor, size);
                }
                else
                {
                    DrawSide(edge, color, size);
                }
            }
            foreach (Vertex vertex in polygon.Vertices())
            {
                if (polygon.EqualsSelected(vertex))
                {
                    DrawVertex(vertex, selectionColor);
                }
                else
                {
                    DrawVertex(vertex, color);
                }
                if(polygon.IsCompleted())DrawAngle(polygon, vertex);
            }
            foreach (Edge edge in polygon.Edges())
            {
                DrawConstraintIcon(edge);
            }
            RefreshImage();
        }
        private void DrawAngle(Polygon polygon, Vertex vertex)
        {
            (float startAngle, float endAngle) = polygon.GetAngle(vertex);
            Pen pen = new Pen(Color.Black, 5);
            Rectangle rectangle = new Rectangle((int)vertex.X - 50, (int)vertex.Y - 50, 100, 100);
            graphics.DrawArc(pen, rectangle, startAngle, (int)(endAngle - startAngle));
            pen.Dispose();
            int angleToDisplay = (int)Math.Round(endAngle - startAngle);
            System.Drawing.Font drawFont = new System.Drawing.Font("TimesNewRomans", 14);
            (double x, double y) = Polygon.PolarToCartesian(30, (startAngle + endAngle) / 2);
            x += vertex.X;
            y += vertex.Y;
            StringFormat stringFromat = new StringFormat();
            stringFromat.Alignment = StringAlignment.Center;
            stringFromat.LineAlignment = StringAlignment.Center;
            SolidBrush solidBrush = new SolidBrush(Color.Blue);
            graphics.DrawString(Math.Abs(angleToDisplay).ToString(), drawFont,solidBrush,new RectangleF((float)x-25,(float) y-25, 50f, 50f),stringFromat);
            solidBrush.Dispose();
        }
        private void DrawConstraintIcon(Edge edge)
        {
            Vertex vertex = edge.GetMidleVertex();
            Vector vector = edge.ToVector().GetPerpendicularVector().ToUnitVector() * iconSize;
            vertex += vector;
            vertex.X -= iconSize / 2;
            vertex.Y -= iconSize / 2;
            switch (edge.Constraint)
            {
                case Constraint.Horizontal:
                    Graphics.FromImage(bitmap).DrawImage(horizontalIcon, new Rectangle(vertex.ToPoint(), new Size(iconSize, iconSize)));
                    break;
                case Constraint.Vertical:
                    Graphics.FromImage(bitmap).DrawImage(verticalIcon, new Rectangle(vertex.ToPoint(), new Size(iconSize, iconSize)));
                    break;
            }
        }
        public void SetBitmap(Bitmap bitmap)
        {
            this.bitmap.Dispose();
            graphics.Dispose();
            this.bitmap = bitmap;
            graphics = Graphics.FromImage(bitmap);
            pictureBox.Image = bitmap;
        }
    }
}
