﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graph_Editor_GK
{
    class Polygon
    {
        public List<Vertex> vertices;
        public List<Edge> edges;
        public Vertex selectedVertex;
        public Edge selectedEdge;

        public Vertex LastVertex { get; set; }
        public Polygon()
        {
            vertices = new List<Vertex>();
            edges = new List<Edge>();
        }
        public IEnumerable EdgesByVertex(Vertex vertex)
        {
            foreach (Edge edge in edges)
            {
                if (edge.V1 == vertex || edge.V2 == vertex) yield return edge;
            }
        }
        public PolygonClickInfo GetPolygonClickInfo()
        {
            if (selectedEdge != null)
            {
                PolygonClickInfo polygonClickInfo = new PolygonClickInfo(EdgeOrVertex.Edge, false, selectedEdge.Constraint);
                if (CheckOrientation(Constraint.Horizontal)) polygonClickInfo.AddConstraint(Constraint.Horizontal);
                if (CheckOrientation(Constraint.Vertical)) polygonClickInfo.AddConstraint(Constraint.Vertical);
                return polygonClickInfo;
            }
            if (selectedVertex != null)
            {
                PolygonClickInfo polygonClickInfo = new PolygonClickInfo(EdgeOrVertex.Vertex, vertices.Count > 3 ? true : false, selectedVertex.Constraint);
                if (CheckAngle()) polygonClickInfo.AddConstraint(Constraint.Angle);
                return polygonClickInfo;
            }
            return new PolygonClickInfo(EdgeOrVertex.None, false, Constraint.None);

            bool CheckOrientation(Constraint constraint)
            {
                Edge e1 = FirstEdge(selectedEdge.V2);
                Edge e2 = SecondEdge(selectedEdge.V1);
                if (e1.Constraint!=constraint&&e2.Constraint!=constraint)
                {
                    
                    if (e1.Constraint != Constraint.None && selectedEdge.V2.Constraint == Constraint.Angle) return false;
                    if (e2.Constraint != Constraint.None && selectedEdge.V1.Constraint == Constraint.Angle) return false;
                    return true;
                }
                return false; 
            }
            bool CheckAngle()
            {
                if (FirstEdge(selectedVertex).Constraint == Constraint.None) return true;
                if (SecondEdge(selectedVertex).Constraint == Constraint.None) return true;
                return false;
            }
        }
        public IEnumerable Vertices()
        {
            for (int i = 0; i < vertices.Count; i++)
            {
                yield return vertices[i];
            }
        }
        public IEnumerable Edges()
        {
            for (int i = 0; i < edges.Count; i++)
            {
                yield return edges[i];
            }
        }
        public bool EqualsSelected(Vertex vertex)
        {
            return selectedVertex == vertex;
        }
        public bool EqualsSelected(Edge edge)
        {
            return selectedEdge == edge;
        }
        public void Select(int x, int y)
        {
            selectedVertex = null;
            selectedEdge = null;
            bool selectionChanged = false;
            foreach (Vertex vertex in vertices)
            {
                if (vertex.CloseToVertex(x, y))
                {
                    selectedVertex = vertex;
                    selectionChanged = true;
                    break;
                }
            }
            if (!selectionChanged)
                foreach (Edge edge in edges)
                {
                    if (edge.CloseToEdge(x, y))
                    {
                        selectedEdge = edge;
                        break;
                    }
                }
        }
        public void AddVertex(int x, int y)
        {
            if (vertices.Count > 2)
            {
                if (vertices[0].CloseToVertex(x, y))
                {
                    edges.RemoveAt(edges.Count - 1);
                    vertices.RemoveAt(vertices.Count - 1);
                    edges.Add(new Edge(vertices[vertices.Count - 1], vertices[0]));
                    LastVertex = null;
                    return;
                }
            }
            Vertex vertex = new Vertex(x, y);
            LastVertex = vertex;
            if (vertices.Count != 0)
            {
                edges.Add(new Edge(vertices[vertices.Count - 1], vertex));
            }
            vertices.Add(vertex);
        }
        public bool IsCompleted()
        {
            return (LastVertex == null && vertices.Count > 0);
        }
        public void MoveLastVertex(int x, int y)
        {
            LastVertex.X = x;
            LastVertex.Y = y;
        }
        public bool SelectionEstablished()
        {
            return selectedEdge != null || selectedVertex != null;
        }
        public void MoveSelectedItem(Vector v)
        {
            if (selectedVertex != null)
            {
                Edge firstEdge = FirstEdge(selectedVertex);
                Edge secondEdge = SecondEdge(selectedVertex);
                Vector v1 = firstEdge.ToVector();
                Vector v2 = secondEdge.ToVector();
                (Vector c1, Vector c2) = v.GetComponent(v1, v2);
                if(selectedVertex.Constraint==Constraint.Angle)
                {
                    MoveVertex(firstEdge.V2, c2, true);
                    MoveVertex(secondEdge.V1, c1, false);
                }
                if(firstEdge.V2.Constraint==Constraint.Angle)
                {
                    MoveVertex(firstEdge.V2, c2, true);
                }
                if(secondEdge.V1.Constraint==Constraint.Angle)
                {
                    MoveVertex(secondEdge.V1, c1, false);
                }
                else
                {
                    if (firstEdge.Constraint != Constraint.None) MoveVertex(firstEdge.V2, c2, true);
                    if (secondEdge.Constraint != Constraint.None) MoveVertex(secondEdge.V1, c1, false);
                }
                selectedVertex += v;
            }
            if (selectedEdge != null)
            {
                Edge firstEdge = FirstEdge(selectedEdge.V2);
                Edge secondEdge = SecondEdge(selectedEdge.V1);
                Vector v1 = firstEdge.ToVector();
                Vector v2 = secondEdge.ToVector();
                Vector v3 = selectedEdge.ToVector();

                (Vector c1, Vector c2) = v.GetComponent(v1, v3);
                (Vector c3, Vector c4) = v.GetComponent(v2, v3);
                MoveVertex(firstEdge.V2, c2, true);
                MoveVertex(secondEdge.V1, c4, false);
                selectedEdge.V1 += v;
                selectedEdge.V2 += v;
            }
        }
        private void MoveVertex(Vertex vertex, Vector v, bool? asc)
        {
            Edge firstEdge = FirstEdge(vertex);
            Edge secondEdge = SecondEdge(vertex);
            Vector v1 = firstEdge.ToVector();
            Vector v2 = secondEdge.ToVector();
            (Vector c1, Vector c2) = v.GetComponent(v1, v2);
            if (asc == true)
            {
                if (firstEdge.Constraint != Constraint.None && !v.CloseToNone()) MoveVertex(firstEdge.V2, c2, asc);
                if (firstEdge.V2.Constraint != Constraint.None && !v.CloseToNone()) MoveVertex(firstEdge.V2, c2, asc);
            }
            else
            {
                if (secondEdge.Constraint != Constraint.None && !v.CloseToNone()) MoveVertex(secondEdge.V1, c1, asc);
                if (secondEdge.V1.Constraint != Constraint.None && !v.CloseToNone()) MoveVertex(secondEdge.V1, c1, asc);
            }
            vertex += v;

        }
        public void RegainConstraints()
        {
            foreach (Edge edge in edges)
            {
                if (edge.Constraint == Constraint.Horizontal && edge.V1.Y != edge.V2.Y)
                {
                    double position = (edge.V1.Y + edge.V2.Y) / 2;
                    edge.V1.Y = position;
                    edge.V2.Y = position;
                    if (edge.V1.X == edge.V2.X) edge.V2.X++;
                }
                if (edge.Constraint == Constraint.Vertical && edge.V1.X != edge.V2.X)
                {
                    double position = (edge.V1.X + edge.V2.X) / 2;
                    edge.V1.X = position;
                    edge.V2.X = position;
                    if (edge.V1.Y == edge.V2.Y) edge.V2.Y++;
                }
            }
        }
        private Edge FirstEdge(Vertex vertex)
        {
            foreach (Edge edge in edges)
            {
                if (edge.V1 == vertex) return edge;
            }
            return null;
        }
        private Edge SecondEdge(Vertex vertex)
        {
            foreach (Edge edge in edges)
            {
                if (edge.V2 == vertex) return edge;
            }
            return null;
        }
       
        public void RemoveVertex()
        {
            vertices.Remove(selectedVertex);
            int i = 0;
            while (edges[i].V1 != selectedVertex) i = (i + 1 % edges.Count);
            Vertex end = edges[i].V2;
            edges.RemoveAt(i);
            i = i % edges.Count;
            while (edges[i].V2 != selectedVertex) i = (i + 1) % edges.Count;
            Vertex start = edges[i].V1;
            edges.RemoveAt(i);
            i = i % edges.Count;
            edges.Insert(i, new Edge(start, end));
            selectedVertex = null;
        }

        public void AddVertexOnEdge()
        {
            Vertex vertex = selectedEdge.GetMidleVertex();
            int edgeIndex = edges.IndexOf(selectedEdge);
            edges.Insert(edgeIndex, new Edge(vertex, selectedEdge.V2));
            edges.Insert(edgeIndex, new Edge(selectedEdge.V1, vertex));
            int vertexIndex = vertices.IndexOf(selectedEdge.V2);
            vertices.Insert(vertexIndex, vertex);
            edges.Remove(selectedEdge);
            selectedEdge = null;
        }

        public void AddConstraint(Constraint constraint)
        {
            if (selectedEdge != null) selectedEdge.Constraint = selectedEdge.Constraint == constraint ? Constraint.None : constraint;
            if (selectedVertex != null)
            {
                selectedVertex.Constraint = selectedVertex.Constraint == constraint ? Constraint.None : constraint; ;
            }
            RegainConstraints();
        }




        public (float, float) GetAngle(Vertex vertex)
        {
            Edge firstEdge = FirstEdge(vertex);
            Edge secondEdge = SecondEdge(vertex);
            double x = secondEdge.V1.X - vertex.X;
            double y = secondEdge.V1.Y - vertex.Y;
            double startAngle = FindDegree(y, x);
            x = firstEdge.V2.X - vertex.X;
            y = firstEdge.V2.Y - vertex.Y;
            double endAngle = FindDegree(y, x);
            if (endAngle < startAngle) endAngle += 360;
            float testAngle = (float)(startAngle + endAngle) / 2;
            (double tx, double ty) = Polygon.PolarToCartesian(3, testAngle);
            tx += vertex.X;
            ty += vertex.Y;
            if (!PointInsidePolygon((int)Math.Round(tx, 0), (int)Math.Round(ty)))
            {
                double helpAngle = 360-Math.Abs(endAngle - startAngle);
                startAngle = endAngle;
                endAngle = startAngle + helpAngle;

            }
            return ((float)startAngle, (float)endAngle);
            float FindDegree(double x1, double y1)
            {
                float value = (float)((Math.Atan2(x1, y1) / Math.PI) * 180f);
                if (value < 0) value += 360f;
                return value;
            }

        }
        static public (double, double) PolarToCartesian(Double radius, Double angleDegree)
        {
            double angleRadian = angleDegree * 2 * Math.PI / 360;
            double x = radius * Math.Cos(angleRadian);
            double y = radius * Math.Sin(angleRadian);
            return (x, y);
        }
        public bool PointInsidePolygon(int x, int y)
        {
            Vertex vertex1 = new Vertex(x, y);
            Vertex vertex2 = new Vertex(9999,9999);
            Edge edge = new Edge(vertex1, vertex2);
            int count = 0;
            foreach (Edge e in Edges())
            {
                if (Edge.CrosingCheck(edge, e)) count++;
            }
            if (count % 2 == 1) return true;
            else return false;
        }
        static public Polygon TEST()
        {
            Polygon polygon = new Polygon();
            Vertex v1 = new Vertex(50, 50);
            Vertex v2 = new Vertex(50, 250);
            Vertex v3 = new Vertex(250, 250);
            Vertex v4 = new Vertex(250, 50);
            Edge e1 = new Edge(v1, v2);
            Edge e2 = new Edge(v2, v3);
            Edge e3 = new Edge(v3, v4);
            Edge e4 = new Edge(v4, v1);
            polygon.vertices.Add(v1);
            polygon.vertices.Add(v2);
            polygon.vertices.Add(v3);
            polygon.vertices.Add(v4);

            polygon.edges.Add(e1);
            polygon.edges.Add(e2);
            polygon.edges.Add(e3);
            polygon.edges.Add(e4);
            e1.Constraint = Constraint.Vertical;
            e2.Constraint = Constraint.Horizontal;
            e3.Constraint = Constraint.Vertical;
            e4.Constraint = Constraint.Horizontal;
            return polygon;
        }

    }

    public enum EdgeOrVertex { Edge, Vertex, None };
    public enum Constraint { Horizontal, Vertical, Angle, None }
    public struct PolygonClickInfo
    {
        public EdgeOrVertex SelectedObject { get; }
        public bool VertexOverThree { get; }
        public Constraint CurrentConstraint { get; }
        public List<Constraint> allowedConstraint;

        public PolygonClickInfo(EdgeOrVertex selectedObject, bool vertexOverThree, Constraint currentConstraint)
        {
            this.SelectedObject = selectedObject;
            VertexOverThree = vertexOverThree;
            this.CurrentConstraint = currentConstraint;
            allowedConstraint = new List<Constraint>();
        }
        public void AddConstraint(Constraint constraint)
        {
            allowedConstraint.Add(constraint);
        }
    }
}
