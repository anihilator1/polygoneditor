﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graph_Editor_GK
{
    public partial class Form1 : Form
    {
        private DrawingMenager drawingMenager;
        private Color color = Color.Black;
        private float penSize = 4;
        private Polygon currentPolygon;
        private int oldX;
        private int oldY;
        private bool moveInProgres=false;
        private ContextMenu vertexContextMenu;
        private ContextMenu edgeContextMenu;
        
        public Form1()
        {
            InitializeComponent();
            InitDrawing();
            InitEventsHandlers();
            InitContextMenu();
            InitWindowProperties();
            //currentPolygon = Polygon.TEST(); 
        }
        private void InitWindowProperties()
        {
            this.MinimumSize = new Size(1200, 800);
            this.MaximumSize = new Size(1200, 800);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.StartPosition = FormStartPosition.CenterScreen;
        }
        private void InitDrawing()
        {
            int x = drawingSpace.ClientRectangle.Width;
            int y = drawingSpace.ClientRectangle.Height;
            Bitmap bitmap = new Bitmap(x, y);
            drawingSpace.Image = bitmap;
            drawingMenager = new DrawingMenager(drawingSpace);
            DoubleBuffered = true;
        }
        private void InitContextMenu()
        {
            MenuItem menuItem;
            vertexContextMenu = new ContextMenu();
            menuItem=new MenuItem("Remove Vertex");
            menuItem.Click += RemoveVertexClick;
            vertexContextMenu.MenuItems.Add(menuItem);
            menuItem = new MenuItem("Const Angle");
            menuItem.Click += AddAngleConstraint;
            vertexContextMenu.MenuItems.Add(menuItem);

            edgeContextMenu = new ContextMenu();
            menuItem = new MenuItem("Add Vertex");
            menuItem.Click += AddVertexClick;
            edgeContextMenu.MenuItems.Add(menuItem);
            menuItem = new MenuItem("Vertical");
            menuItem.Click += AddVerticalConstraint;
            edgeContextMenu.MenuItems.Add(menuItem);
            menuItem = new MenuItem("Horizontal");
            menuItem.Click += AddHorizontalConstraint;
            edgeContextMenu.MenuItems.Add(menuItem);
        }
        private void InitEventsHandlers()
        {
            drawingSpace.MouseDown += drawingSpace_MouseDown;
            drawingSpace.MouseMove += drawingSpace_MouseMove;
            drawingSpace.MouseUp += drawingSpace_MouseUp;
            drawingSpace.SizeChanged += drawingSpace_SizeChanged;
        }
        private void AddVerticalConstraint(object sender,EventArgs args)
        {
            currentPolygon.AddConstraint(Constraint.Vertical);
        }
        private void AddHorizontalConstraint(object sender,EventArgs args)
        {
            currentPolygon.AddConstraint(Constraint.Horizontal);
        }
        private void AddAngleConstraint(object sender,EventArgs args)
        {
            currentPolygon.AddConstraint(Constraint.Angle);
        }

        private void AddVertexClick(object sender,EventArgs args)
        {
            currentPolygon.AddVertexOnEdge();
        }
        private void RemoveVertexClick(object sender,EventArgs args)
        {
            currentPolygon.RemoveVertex();
        }
      
        private void drawingSpace_MouseDown(object sender, MouseEventArgs args)
        {

            if (args.Button == MouseButtons.Left)
            {
                if (currentPolygon == null || !currentPolygon.IsCompleted())
                {
                    ClickToDraw(args.X, args.Y);
                }
                if(currentPolygon!=null && currentPolygon.SelectionEstablished() )
                {
                    moveInProgres = true;
                    oldX = args.X;
                    oldY = args.Y;
                }
            }
            if(args.Button==MouseButtons.Right)
            {
                currentPolygon.Select(args.X,args.Y);
                PolygonClickInfo polygonClickInfo = currentPolygon.GetPolygonClickInfo();
                drawingSpace.ContextMenu = PrepareContextMenu(polygonClickInfo);
            }
        }
        private void drawingSpace_SizeChanged(object sender,EventArgs args)
        {
            int x = drawingSpace.ClientRectangle.Width;
            int y = drawingSpace.ClientRectangle.Height;
            Bitmap bitmap = new Bitmap(x, y);
            drawingMenager.SetBitmap(bitmap);
            drawingMenager.DrawPolygon(currentPolygon, color, penSize);
        }
        private ContextMenu PrepareContextMenu(PolygonClickInfo polygonClickInfo)
        {
            ContextMenu contextMenu=null;
            if(polygonClickInfo.SelectedObject==EdgeOrVertex.Edge)
            {
                contextMenu = edgeContextMenu;
                contextMenu.MenuItems[1].Enabled = polygonClickInfo.allowedConstraint.Contains(Constraint.Vertical);
                contextMenu.MenuItems[2].Enabled = polygonClickInfo.allowedConstraint.Contains(Constraint.Horizontal);
                contextMenu.MenuItems[1].Checked = polygonClickInfo.CurrentConstraint == Constraint.Vertical;
                contextMenu.MenuItems[2].Checked = polygonClickInfo.CurrentConstraint == Constraint.Horizontal;
            }
            if(polygonClickInfo.SelectedObject==EdgeOrVertex.Vertex)
            {
                contextMenu = vertexContextMenu;
                contextMenu.MenuItems[0].Enabled = polygonClickInfo.VertexOverThree;
                contextMenu.MenuItems[1].Enabled = polygonClickInfo.allowedConstraint.Contains(Constraint.Angle);
                contextMenu.MenuItems[1].Checked = polygonClickInfo.CurrentConstraint == Constraint.Angle;
            }
            if(polygonClickInfo.SelectedObject==EdgeOrVertex.None)
            {
               contextMenu = new ContextMenu();
                MenuItem menuItem = new MenuItem();
                menuItem.Click += TEST;
                contextMenu.MenuItems.Add(menuItem);
            }
            return contextMenu;
        }
        private void TEST(object sender,EventArgs args)
        {
            currentPolygon.selectedEdge = currentPolygon.edges[2];
            
            currentPolygon.MoveSelectedItem(new Vector(150,150));
        }
        private void drawingSpace_MouseUp(object sender,MouseEventArgs args)
        {
            if(args.Button==MouseButtons.Left)
            {
                if(currentPolygon!=null&&currentPolygon.SelectionEstablished())
                {
                    moveInProgres = false;
                    
                }
            }
        }
        void ClickToDraw(int x, int y)
        {
            if (currentPolygon == null)
            {
                currentPolygon = new Polygon();
                currentPolygon.AddVertex(x, y);
            }
            currentPolygon.AddVertex(x, y);
            drawingMenager.DrawPolygon(currentPolygon, color, penSize);
        }
      
  
        private void drawingSpace_MouseMove(object sender, MouseEventArgs args)
        {
            if(currentPolygon!=null)
            {
                if(!currentPolygon.IsCompleted())
                {
                    currentPolygon.MoveLastVertex(args.X, args.Y);
                    drawingMenager.DrawPolygon(currentPolygon, color, penSize);

                }
                else
                {
                    if(!moveInProgres)
                    {
                        currentPolygon.Select(args.X, args.Y);
                    }
                    else
                    {
                        currentPolygon.MoveSelectedItem(new Vector(args.X-oldX,args.Y-oldY));
                        oldX = args.X;
                        oldY = args.Y;
                    }
                    drawingMenager.DrawPolygon(currentPolygon, color, penSize);
                }
               
            }
           
        }
        
    }
}
