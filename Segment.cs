﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Graph_Editor_GK
{
    class Edge
    {
        public Vertex V1 { get; set; }
        public Vertex V2 { get; set; }
        public double a
        {
            get
            {
                return ((double)(V1.Y - V2.Y)) / (V1.X - V2.X);
            }
        }
        private double b;
        public Constraint Constraint { get; set; }

        private int tolerance = 10;
        public Edge(Vertex v1, Vertex v2)
        {
            this.V1 = v1;
            this.V2 = v2;
            Constraint = Constraint.None;
        }

        public Vector ToVector()
        {
            return new Vector(V2.X - V1.X, V2.Y - V1.Y);
        }
        public Vertex GetMidleVertex()
        {
            Vector vector = this.ToVector();
            vector /= 2;
            Vertex vertex = V1.Clone();
            return vertex + vector;
        }
        public bool CloseToEdge(int x, int y)
        {
            if (V1.X == V2.X)
            {
                if (y < Math.Max(V1.Y, V2.Y) && y > Math.Min(V1.Y, V2.Y) && Math.Abs(V1.X - x) <= tolerance)
                {
                    return true;
                }
                return false;
            }
            if (V1.Y == V2.Y)
            {
                if (x < Math.Max(V1.X, V2.X) && x > Math.Min(V1.X, V2.X) && Math.Abs(V1.Y - y) <= 2 * tolerance)
                {
                    return true;
                }
                return false;
            }
            if (!inSquare()) return false;
            double a = ((double)(V1.Y - V2.Y)) / (V1.X - V2.X);
            double b = V1.Y - a * V1.X;
            double result = Math.Abs(a * x - y + b) / Math.Sqrt(Math.Pow(a, 2) + Math.Pow(-1, 2));
            if (result < tolerance) return true;
            return false;

            bool inSquare()
            {
                double hX = (V1.X >= V2.X) ? V1.X : V2.X;
                double lX = (V1.X >= V2.X) ? V2.X : V1.X;
                double hY = (V1.Y >= V2.Y) ? V1.Y : V2.Y;
                double lY = (V1.Y >= V2.Y) ? V2.Y : V1.Y;
                if (x <= hX + tolerance / 2 && x >= lX - tolerance / 2 && y <= hY + tolerance / 2 && y >= lY - tolerance / 2) return true;
                return false;
            }
        }
        public static bool CrosingCheck(Edge edge1, Edge edge2)
        {
            Vertex v1 = edge1.V1;
            Vertex v2 =edge1.V2;
            Vertex v3 = edge2.V1;
            Vertex v4 = edge2.V2;
            double d1 = Vertex.CrossProduct((v4 - v3), (v1 - v3));
            double d2 = Vertex.CrossProduct((v4 - v3), (v2 - v3));
            double d3 = Vertex.CrossProduct((v2 - v1), (v3 - v1));
            double d4 = Vertex.CrossProduct((v2 - v1), (v4 - v1));
            double d12 = d1 * d2;
            double d34 = d3 * d4;
            if (d12 > 0 || d34 > 0) return false;
            if (d12 < 0 || d34 < 0) return true;
            if (OnRectangle(v1, v3, v4) || OnRectangle(v2, v3, v4) || OnRectangle(v3, v1, v2) || OnRectangle(v4, v1, v2))
            {
                if (v1 == v3 && (!OnRectangle(v4, v1, v2) && !OnRectangle(v2, v3, v4))) return true;
                if (v1 == v4 && (!OnRectangle(v3, v1, v2) && !OnRectangle(v2,v3, v4))) return true;
                if (v2 == v3 && (!OnRectangle(v4, v1, v2) && !OnRectangle(v1, v3, v4))) return true;
                if (v2 == v4 && (!OnRectangle(v3, v1, v2) && !OnRectangle(v1, v3, v4))) return true;


                if (v1 == v3 && Vertex.CrossProduct(v2 - v1, v4 - v1) != 0) return false;
                if (v1 == v4 && Vertex.CrossProduct(v2 - v1, v3 - v1) != 0) return false;
                if (v2 == v3 && Vertex.CrossProduct(v1 - v2, v4 - v2) != 0) return false;
                if (v2 == v4 && Vertex.CrossProduct(v1 - v2, v3 - v2) != 0) return false;

                return true;
            }

            return false;
        }
        private static bool OnRectangle(Vertex q, Vertex p1, Vertex p2)
        {
            double x = q.X;
            double y = q.Y;
            double x1 = p1.X;
            double x2 = p2.X;
            double y1 = p1.Y;
            double y2 = p2.Y;
            return Math.Min(x1, x2) <= x && x <= Math.Max(x1, x2) && Math.Min(y1, y2) <= y && y <= Math.Max(y1, y2);
        }
    }
}

